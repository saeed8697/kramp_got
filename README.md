# Welcome To GOT Characters  !

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="https://raw.githubusercontent.com/DXHeroes/knowledge-base-content/master/files/refactoring.png" alt="Logo" width="150px
" height="90">
  </a>

<h3 align="center">Fetching Got Characters in detail   </h3>

  <p align="center">
    for more info 
    <br />
    <a href="https://www.anapioficeandfire.com/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    ·
    <a href="https://www.anapioficeandfire.com/Documentation">Ref Source </a>
    ·
    <a href="https://www.linkedin.com/in/saeed-hasanabadi-79081b183/">About Me </a>
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#about">About Me </a></li>

  </ol>
</details>

## About The Project

<div id="about-the-project"></div>


The Task is to fetch charecters From [anapioficeandfire](https://www.anapioficeandfire.com/)
> Description
>
> Hi and welcome to Task . Here first we need to fetch data from api showing pagination  and processed our data the goal for this project is to reduce usage of api
> we have 3 main pages 1 is for infinite pagination second is for table pagination and third is for detail page
> we also had to estimate our age of character name through [Agify](https://api.agify.io/)




My Solution for reducing the api call and also improve performance in application is to :

## Step 1 - using redis for Server-side caching

Our Redis cache has been set to cache for 15 seconds (for demonstration purposes), so if we continue to call curl in the
other terminal, we will see our call takes =~1 second until the cache has cleared the expired value.

## Step 2 - React Query library for client side caching

To manage server state on the client, we are using the React Query library so whats the difference  :
** Server state assumes that the frontend application does not "own" the data displayed, we display a "snapshot" of the
data that was retrieved at any given point in time. React Query provides us with tools to help define whether that
server state is "fresh" or "stale" and to make operations and calls based upon our configuration for that.

## Step 3 - Use Callback & USe Memo for Memoization

To prevent unnecessary rendering i use usecallback => to memoize function and useMemo => to memoize value

## Step 4 -Debouncing

we use debouncing on pur search input for handling user inputs

## Step 5 - USe Reducer

we use use reducer for manage our state and ignore unpleasant state changes in app

### Built With

<div id="built-with"></div>

* [Next.js](https://nextjs.org/)
* [Tailwind](https://tailwindcss.com/)
* [JavaScript](https://www.javascript.com/)

## Getting Started

<div id="#getting-started"></div>

## Step 1 - Dependencies

You will need:

* [Git](http://git-scm.com/downloads)
* [node](https://nodejs.org/)
* [yarn](https://yarnpkg.com/en/docs/install) (Optional. Not Required if you use NPM)
* [redis](https://redis.io/) (redis)

Please install them if you don't have them already. 

## Step 1 - Redis 
I use  Upstash for my redis server
you can change it to local if you wish (you need active redis installed on your device here is the link for download [redis](https://redis.io/download/))


## Step 2 - Clone the repository:

<div id="prerequisites"></div>

From the command line, clone the repository:

```sh
$ git clone https://gitlab.com/saeed8697/kramp_got.git
```

## Step 3 Install Each Lesson

Each section is a separate app using [NextJsApp](https://nextjs.org/docs/getting-started). This repo has a script to
install all of them at once.

If you are using yarn run from the root of the repository:

```sh
yarn install 
```

If you are using npm, run from the root of the repository:

```sh
npm run install
```

<div id="installation"></div>

## Step 4 - Run an app on Development

Once the dependencies are installed, you can run the app for a lesson:

```sh
yarn dev
# or
npm run dev
```

## Step 5 - Build your Application

for production and building your application :

```sh
yarn build 
# or
npm run build  



```

## Run app on production -

for run on production just run  :
<div id="testing"></div>

```sh
yarn test 
# or
npm run start  

```

Your browser should open up to a running app.

# Hey there ! Saeed Hasanabadi  ! &emsp;  <img src="https://raw.githubusercontent.com/kvssankar/kvssankar/main/programmer.gif" width="350px">

<div id="about"></div>

### How to reach me: <strong>(Click the badge to view my profiles !)</strong>

<img src="https://img.shields.io/badge/saeedh582@gmail.com-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" href="mailto:saeedh582@gmail.com">   <a  href="https://saeedhasanabadi.herokuapp.com"><img src="https://img.shields.io/badge/@Website -%23E4405F.svg?&style=for-the-badge&logo=website&logoColor=white"></a>   <a href="https://www.linkedin.com/in/saeed-hasanabadi-79081b183/"><img src="https://img.shields.io/badge/saeed-hasanabadi-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" ></a>  
<hr>

#### Thank You-🙏🏼

⭐️ From [Saeed](https://saeedhasanabadi.herokuapp.com)
