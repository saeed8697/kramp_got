import GotUtils from "../utils/GotUtils";

/****
 * component for handling and styling main char card
 * @param charecter
 * @returns {JSX.Element}
 * @constructor
 */
const CharacterCard = ({charecter}) => {
    return (
        <div className="card">
            <div className="box bg-blue-300 dark:bg-dark-elements">
                <div className="content">
                    <h2>{GotUtils.getIdFromUrl(charecter.url)}</h2>
                    <h3>{charecter.name ?? "not set "}</h3>
                    <p>{charecter.playedBy.toString()}</p>
                </div>
            </div>
        </div>
    );
};

export default CharacterCard;
