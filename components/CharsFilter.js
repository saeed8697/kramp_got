import React from 'react'
// import Select from 'react-select'

// const statusOptions = [
//     {'value':'', 'label':'All'},
//     {'value':'Active', 'label':'Active'},
//     {'value':'Inactive', 'label':'Inactive'},
//     {'value':'Pending', 'label':'Pending'},
// ]


//search filter based on cache

const CharsFilter = ({onClickFilterCallback, defaultKeyword}) => {
    const [keyword, setKeyword] = React.useState(defaultKeyword)
    const onKeywordChange = (e) => {
        setKeyword(e.target.value)
    }
    const onClickSearch = (e) => {
        setKeyword(e.target.value)

        onClickFilterCallback(e.target.value)
    }
    return (
        <>
            <label className="flex gap-x-2 items-baseline">
                <span className="text-gray-700">Search:  (in cache) </span>
                <input
                    type="text"
                    className="rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                    value={keyword}
                    onChange={onClickSearch}

                />

            </label>
            <div className="col-md-4 d-flex align-items-center max-height-32px pl-1">
                <span className="text-blue pointer" onClick={onClickSearch}>Search in cache </span>
            </div>
        </>


    )
}

export default CharsFilter;
