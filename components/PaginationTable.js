import React from 'react';
import PropTypes from 'prop-types';
import {PaginationLink,} from 'reactstrap';
import ChevronRightIcon from 'mdi-react/ChevronRightIcon';
import ChevronDoubleRightIcon from 'mdi-react/ChevronDoubleRightIcon';
import ChevronLeftIcon from 'mdi-react/ChevronLeftIcon';
import ChevronDoubleLeftIcon from 'mdi-react/ChevronDoubleLeftIcon';
import {PageButton} from "./shared/Button";


//handle pagination tools like after before and custom page
const PaginationTable = ({
                             dataLength,
                             page,
                             gotoPage,
                             canPreviousPage,
                             pageOptions,
                             pageSize,
                             pageIndex,
                             previousPage,
                             nextPage,
                             canNextPage,
                             setPageSize,
                             manualPageSize,
                         }) => {
    // console.log(pageOptions)
    const arrayPageIndex = (pageIndex - 2) < 0
        ? pageOptions.slice(1, pageIndex + 3)
        : pageOptions.slice((pageIndex - 1), (pageIndex + 3));

    return (
        <div>
            <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                <PageButton
                    className="rounded-l-md"
                    onClick={() => gotoPage(0)}
                    disabled={!canPreviousPage}
                >
                    <span className="sr-only">First</span>
                    <ChevronDoubleLeftIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
                </PageButton>
                <PageButton
                    onClick={() => previousPage()}
                    disabled={!canPreviousPage}
                >
                    <span className="sr-only">Previous</span>
                    <ChevronLeftIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
                </PageButton>
                {arrayPageIndex.map(i => (
                    <PageButton
                        className="pagination__item"
                        disabled={pageIndex === i}
                        key={i}
                    >
                        <div
                            key={i}
                            onClick={() => gotoPage(i)}
                        >
                            <span className="h-5 w-5 text-gray-400">{i}</span>

                        </div>
                    </PageButton>
                ))}
                <PageButton
                    onClick={() => nextPage()}
                    disabled={!canNextPage
                    }>
                    <span className="sr-only">Next</span>
                    <ChevronRightIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
                </PageButton>
                <PageButton
                    className="rounded-r-md"
                    onClick={() => gotoPage(pageOptions.length - 1)}
                    disabled={!canNextPage}
                >
                    <span className="sr-only">Last</span>
                    <ChevronDoubleRightIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
                </PageButton>
            </nav>
        </div>
    );
};

PaginationTable.propTypes = {
    dataLength: PropTypes.number.isRequired,
    page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    gotoPage: PropTypes.func.isRequired,
    canNextPage: PropTypes.bool.isRequired,
    canPreviousPage: PropTypes.bool.isRequired,
    pageOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
    pageSize: PropTypes.number.isRequired,
    pageIndex: PropTypes.number.isRequired,
    previousPage: PropTypes.func.isRequired,
    nextPage: PropTypes.func.isRequired,
    setPageSize: PropTypes.func.isRequired,
    manualPageSize: PropTypes.arrayOf(PropTypes.number),
};


export default PaginationTable;
