import React, {useEffect, useState} from "react";
import {IoMoon, IoSunny} from "react-icons/io5";

/**
 *
 * theme toggle for handling dark and light theme
 * @returns {JSX.Element}
 * @constructor
 */
const Toggle = () => {
    const [dark, setDark] = useState(false);

    const handleSwitcher = () => {
        setDark(!dark);
        localStorage.setItem("dark", !dark);
    }
    useEffect(() => {
        if (localStorage.getItem("dark") === "true") {
            document.body.classList.remove("light");
            document.body.classList.add("dark");
            setDark(true);
        } else {
            document.body.classList.remove("dark");
            document.body.classList.add("light");
            setDark(false);
        }
    }, [dark])


    return (
        <div className="p-2 transition duration-500 ease-in-out rounded-full">
            {dark === "dark" ? (
                <IoSunny
                    onClick={handleSwitcher}
                    className="text-2xl cursor-pointer"
                />
            ) : (
                <IoMoon
                    onClick={handleSwitcher}
                    className="text-2xl cursor-pointer"
                />
            )}
        </div>
    );
};

export default Toggle;
