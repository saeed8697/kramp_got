import React from "react";

//fetch data needed for table
export const fetchTableData = async (page) => {
    const offset = page;
    try {
        const response = await fetch(
            `/api/${page}`
        );
        return await response.json();
    } catch (e) {
        throw new Error(`API error:${e?.message}`);
    }
};

//fetch estimaated age
export const fetchAge = async (name) => {


    if (!name && name.toString() ==='')
        return ''

    try {
        const response = await fetch(
            `https://api.agify.io/?name=${name}`
        );
        return await response.json();
    } catch (e) {
        throw new Error(`API error:${e?.message}`);
    }
};