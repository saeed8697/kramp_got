import React from "react";
import {useInfiniteQuery} from "react-query";

function useInfiniteFetch() {
    // State and setters for debounced value
    const {data, status, fetchNextPage, hasNextPage} = useInfiniteQuery(
        "Chars",
        async ({pageParam = 1}) =>
            await fetch(
                `/api/${pageParam}`
            ).then((result) => result.json()),
        {
            staleTime: 60 * 1000, // 1 minute
            cacheTime: 60 * 1000 * 10, // 10 minutes
            keepPreviousData: true,
            refetchOnMount: false,
            refetchOnWindowFocus: false,
            getNextPageParam: (lastPage, pages) => {
                if (lastPage.items?.next) {
                    return lastPage.items?.next

                }


            },
        }
    );
    return {data, status, fetchNextPage, hasNextPage};
}


export default useInfiniteFetch;