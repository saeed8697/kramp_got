import {QueryClient, useMutation} from "react-query";
import axios from "axios";
import GotUtils from "../utils/GotUtils";

function useMutationDetail() {
    // State and setters for debounced value
    const queryClient = new QueryClient()

    return useMutation(
        //TODO
        character => axios.post(`/api/getChar/${GotUtils.getIdFromUrl(character.url)}`, {character}),
        {
            // Optimistically update the cache value on mutate, but store
            // the old value and return it so that it's accessible in case of
            // an error
            onMutate: async text => {
                await queryClient.cancelQueries('char_detail')

                const previousValue = queryClient.getQueryData('char_detail')

                queryClient.setQueryData('char_detail', old => ({
                    text
                }))

                return previousValue
            },
            // On failure, roll back to the previous value
            onError: (err, variables, previousValue) =>
                queryClient.setQueryData('char_detail', previousValue),
            // After success or failure, refetch the char_detail query
            onSettled: () => {
                queryClient.invalidateQueries('char_detail')
            },
        }
    );
}

export default useMutationDetail;