import React, {useEffect, useMemo} from "react"
import {usePagination, useSortBy, useTable} from "react-table"
import {QueryClient, QueryClientProvider, useQuery} from 'react-query'

import SortIcon from 'mdi-react/SortIcon'
import SortAscendingIcon from 'mdi-react/SortAscendingIcon'
import SortDescendingIcon from 'mdi-react/SortDescendingIcon'
import PaginationTable from '../components/PaginationTable'
import {fetchTableData} from "../hooks/useFetch";
import {classNames} from "../components/shared/Utils";
import GotUtils from "../utils/GotUtils";
import Link from "next/link";
import useMutationDetail from "../hooks/usseMutation";
import {SortDownIcon, SortUpIcon} from "../components/shared/Icons";


// custom cell for died
export function DiedStatus({value}) {
    const status = value ? value.toLowerCase() : "NO";

    return (<span
        className={classNames("px-3 py-1 uppercase leading-wide font-bold text-xs rounded-full shadow-sm", status.startsWith("NO") ? "bg-green-100 text-green-800" : "bg-red-100 text-red-800",)}
    >
      {status}
    </span>);
}

//custom cell for id
export function IdCell({value}) {

    return (<span
        className={classNames("px-6 py-4 whitespace-nowrap")}>

      {GotUtils.getIdFromUrl(value)}
    </span>);
}

const queryClient = new QueryClient()


const USERS_COLUMNS = [
    {
        Header: 'Url',
        accessor: 'url',
        Cell: IdCell
    },
    {
        Header: 'Name',
        accessor: 'name',
    }, {
        Header: 'Aliases',
        accessor: 'aliases',
    }, {
        Header: 'Died',
        accessor: 'died',
        Cell: DiedStatus
    },
    {
        width: 100,
        Header: "Detail",
        Cell: (props) => {
            const row = props.cell.original

            const url = GotUtils.getIdFromUrl(row.url)

            return (
                <Link

                    href={`/characters/${url}/?name=${row.name}&flag=/TablePaginate`}


                    key={url}

                >

                    <a onClick={() => {
                        props.mutation.mutate(row)
                    }}>
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full">
                            Detail
                        </button>
                    </a>


                </Link>
            )


        },
    }

];
//reducer initial state
const initialState = {
    queryPageIndex: 1, queryPageSize: 10, totalCount: 0, queryPageFilter: "", queryPageSortBy: [],
};
//trimming data


// reducers states
const PAGE_CHANGED = 'PAGE_CHANGED';
const PAGE_SIZE_CHANGED = 'PAGE_SIZE_CHANGED';
const PAGE_SORT_CHANGED = 'PAGE_SORT_CHANGED';
const PAGE_FILTER_CHANGED = 'PAGE_FILTER_CHANGED';
const TOTAL_COUNT_CHANGED = 'TOTAL_COUNT_CHANGED';

const reducer = (state, {type, payload}) => {
    switch (type) {
        case PAGE_CHANGED:
            return {
                ...state, queryPageIndex: payload,
            };
        case PAGE_SIZE_CHANGED:
            return {
                ...state, queryPageSize: payload,
            };
        case PAGE_SORT_CHANGED:
            return {
                ...state, queryPageSortBy: payload,
            };
        case PAGE_FILTER_CHANGED:
            return {
                ...state, queryPageFilter: payload,
            };
        case TOTAL_COUNT_CHANGED:
            return {
                ...state, totalCount: payload,
            };
        default:
            throw new Error(`Unhandled action type: ${type}`);
    }
};


const DataTable = () => {
    //use mutation for handling our states of clicking
    const addTodoMutation = useMutationDetail()


    //memoized our column values
    let columns = useMemo(() => USERS_COLUMNS, [])


    //use reducer for handing our states
    const [{
        queryPageIndex, queryPageSize, totalCount, queryPageFilter, queryPageSortBy
    }, dispatch] = React.useReducer(reducer, initialState);


    //use query for fetch data n pagination
    const {
        isLoading, error, data, isSuccess
    } = useQuery(['Chars', queryPageIndex, queryPageSize, queryPageFilter, queryPageSortBy], () => fetchTableData(queryPageIndex), {
        keepPreviousData: true, staleTime: Infinity,
    });

    const totalPageCount = Math.ceil(totalCount / queryPageSize)


    //react table for handling pagination
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        page,
        pageCount,
        pageOptions,
        gotoPage,
        previousPage,
        canPreviousPage,
        nextPage,
        canNextPage,
        setPageSize,
        state: {pageIndex, pageSize, sortBy}
    } = useTable({
        columns,
        data: isSuccess ? (data.items.items) : [],
        initialState: {
            pageIndex: queryPageIndex, pageSize: queryPageSize, sortBy: queryPageSortBy,
        },
        manualPagination: true,
        pageCount: isSuccess ? Math.ceil(totalCount / queryPageSize) : null,
        autoResetSortBy: false,
        autoResetExpanded: false,
        autoResetPage: false
    }, useSortBy, usePagination,);


    //---------------
    const manualPageSize = []


    //use effect for handling our reducers changes
    useEffect(() => {
        dispatch({type: PAGE_CHANGED, payload: pageIndex});
    }, [pageIndex]);

    useEffect(() => {
        dispatch({type: PAGE_SIZE_CHANGED, payload: pageSize});
        gotoPage(0);
    }, [pageSize, gotoPage]);

    useEffect(() => {
        dispatch({type: PAGE_SORT_CHANGED, payload: sortBy});
        gotoPage(0);
    }, [sortBy, gotoPage]);


    React.useEffect(() => {
        if (data?.items.last) {
            dispatch({
                type: TOTAL_COUNT_CHANGED, payload: data.items.last * 10,
            });
        }
    }, [data?.items.last]);

    if (error) {
        return <p>Error</p>;
    }

    if (isLoading) {
        return <p>Loading...</p>;
    }

    if (isSuccess) return (<div className="min-h-screen  text-gray-900">

            <div className="max-w-8xl mx-auto px-4 sm:px-6 lg:px-8 pt-4">

                <div className="mt-6">
                    <form className="sm:flex sm:gap-x-2">
                        <div className="form__form-group">

                            {headerGroups.map((headerGroup) => headerGroup.headers.map((column) => column.Filter ? (
                                <div className="mt-2 sm:mt-0" key={column.id}>
                                    {column.render("Filter")}
                                </div>) : null))}
                        </div>
                    </form>

                    {isLoading &&
                        <p>Loading...</p>}
                    <div className="py-3 flex-nowrap items-center justify-between">
                        <div className="-my-2 overflow-x-auto -mx-4 sm:-mx-6 lg:-mx-8">
                            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                    <table {...getTableProps()} className="min-w-full divide-y divide-gray-200">
                                        <thead className="bg-gray-50">
                                        {headerGroups.map(headerGroup => (<tr {...headerGroup.getHeaderGroupProps()}>
                                            {headerGroup.headers.map(column => (// Add the sorting props to control sorting. For this example
                                                // we can add them into the header props
                                                <th
                                                    scope="col"
                                                    className="group px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    {...column.getHeaderProps(column.getSortByToggleProps())}
                                                >
                                                    <div className="flex items-center justify-between">
                                                        {column.render('Header')}
                                                        {/* Add a sort direction indicator */}
                                                        <span>
                              {column.isSorted ? column.isSortedDesc ?
                                  <SortDownIcon className="w-4 h-4 text-gray-400"/> :
                                  <SortUpIcon className="w-4 h-4 text-gray-400"/> : (
                                  <SortIcon className="w-4 h-4 text-gray-400 opacity-0 group-hover:opacity-100"/>)}
                            </span>
                                                    </div>
                                                </th>))}
                                        </tr>))}
                                        </thead>
                                        <tbody
                                            {...getTableBodyProps()}
                                            className="bg-white dark:bg-blue-200 dark:text-blue-800 divide-y divide-gray-200"
                                        >
                                        {page.map((row, i) => {  // new
                                            prepareRow(row)
                                            return (

                                                <tr onClick={() => addTodoMutation.mutate(row)} {...row.getRowProps()}>
                                                    {row.cells.map(cell => {
                                                        return (<td
                                                            {...cell.getCellProps()}
                                                            className="px-6 py-4 whitespace-nowrap"
                                                            role="cell"
                                                        >
                                                            {cell.column.Cell.name === "defaultRenderer" ? <div
                                                                className="text-sm text-gray-500">{cell.render('Cell')}</div> : cell.render('Cell', {
                                                                cell: row,
                                                                mutation: addTodoMutation
                                                            })}
                                                        </td>)
                                                    })}
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    {(rows.length > 0) && (<div className="py-3 flex items-center justify-between">
                            <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                                <PaginationTable
                                    page={page}
                                    gotoPage={gotoPage}
                                    previousPage={previousPage}
                                    nextPage={nextPage}
                                    canPreviousPage={canPreviousPage}
                                    canNextPage={canNextPage}
                                    pageOptions={pageOptions}
                                    pageSize={pageSize}
                                    pageIndex={pageIndex}
                                    pageCount={pageCount}
                                    setPageSize={setPageSize}
                                    manualPageSize={manualPageSize}
                                    dataLength={totalCount}
                                />


                                <div className="flex gap-x-2 items-baseline">
                                       <span className="text-sm text-gray-700 dark:text-blue-800">
                                                Page <span className="font-medium">{pageIndex + 1}</span> of <span
                                                      className="font-medium">{pageOptions.length}</span>
                                       </span>
                                    <span className="text-sm text-gray-700 ml-2 mr-1 dark:text-blue-800 ">
                                            Go To Page :

                                   </span>
                                    <label>

                                        <input
                                            type="number"
                                            value={pageIndex + 1}
                                            onChange={(e) => {
                                                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                                gotoPage(page);
                                            }}
                                            className="mt-1 pt-2 pb-2 pl-3  block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                        />
                                    </label>


                                </div>

                            </div>
                        </div>


                    )}
                </div>
            </div>
        </div>

    )
}

const Sorting = ({column}) => (<span className="react-table__column-header sortable">
      {column.isSortedDesc === undefined ? (<SortIcon/>) : (<span>
          {column.isSortedDesc ? <SortAscendingIcon/> : <SortDescendingIcon/>}
        </span>)}
    </span>);

const TableWrapper = () => {
    return (<QueryClientProvider client={queryClient}>
        <DataTable/>
    </QueryClientProvider>)
}

export default TableWrapper;
