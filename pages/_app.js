import {useEffect, useState} from "react";
import "../styles/globals.css";
import "../styles/tailwind.css";
import 'nprogress/nprogress.css';
import nProgress from 'nprogress'
import * as ga from '../lib/ga'
import {useRouter} from 'next/router'
import {Hydrate, QueryClient, QueryClientProvider} from "react-query";
import {ReactQueryDevtools} from "react-query/devtools";
import {persistQueryClient} from "react-query/persistQueryClient-experimental";
import {createWebStoragePersistor} from "react-query/createWebStoragePersistor-experimental";
import Navbar from "../components/Navbar";

const queryClient = new QueryClient();


//persistQueryClient is a utility for persisting the state of your queryClient and its caches for later use
if (typeof window !== "undefined") {
    const localStoragePersistor = createWebStoragePersistor({
        storage: window.localStorage,
    });

    persistQueryClient({
        queryClient,
        persistor: localStoragePersistor,
    });
}


function MyApp({Component, pageProps}) {
    const [isMounted, setIsMounted] = useState(false);
    const router = useRouter()

    useEffect(() => {
        setIsMounted(true);
        const handleRouteChange = (url) => {
            nProgress.done

            ga.pageview(url)
        }
        //When the component is mounted, subscribe to router changes
        //and log those page views
        router.events.on('routeChangeComplete', handleRouteChange)
        router.events.on('routeChangeComplete', nProgress.done)
        router.events.on('routeChangeStart', nProgress.start)
        router.events.on('routeChangeError', nProgress.done)
        // If the component is unmounted, unsubscribe
        // from the event with the `off` method
        return () => {
            router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [router.events])


    return (
        isMounted && (
            <QueryClientProvider client={queryClient}>

                <Hydrate state={pageProps.dehydratedState}>
                    <Navbar/>
                    <Component {...pageProps} />
                </Hydrate>

                <ReactQueryDevtools initialIsOpen={false}/>
            </QueryClientProvider>

        )
    );
}

export default MyApp;
