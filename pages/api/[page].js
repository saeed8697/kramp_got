// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import {performance} from "perf_hooks";
import redis from "redis";
import util from "util";
import IceAndFireApiService from "../../services/IceAndFireApiService";

var client = redis.createClient ({
    host : 'global-tidy-escargot-30186.upstash.io',
    port : '30186',
    password: '9a5e20f37e5e45c38a10d64d1b7b34a5',
    tls: {
        rejectUnauthorized: false
    }
});

// Promisify the get command so we can use async/await.
client.get = util.promisify(client.get);


/**
 *
 * implemmnt redis for caching on pagination for 15 sec
 * @param req is page number
 * @param res is charactes from ice Api
 * @returns {Promise<void>}
 */
export default async function handler(req, res) {
    try {
        const {page} = req.query;


        const startTime = performance.now();
        const response = await client.get(page);

        if (response) {
            console.log('page : ', page, ' loaded  from cache ')

            // This value is considered fresh for 15 seconds \
            // If a request is repeated within the next 15 seconds, the previously
            // cached value will still be fresh. If the request is repeated before 15 seconds,
            // the cached value will be stale but still render (stale-while-revalidate=15).
            //
            // In the background, a revalidation request will be made to populate the cache
            // with a fresh value. If you refresh the page, you will see the new value.
            res.setHeader("Cache-Control", "public, max-age=5, immutable");
            res.setHeader("X-Cache", "HIT");
            res.status(200).json(JSON.parse(response));
        } else {
            // As a contrived, let's say this is the expected result from the database
            const data = await IceAndFireApiService.getResultOfSpi(page, 10);


            // Here we are caching the result for 15 seconds to Redis
            client.setex(page, 15, JSON.stringify(data));

            // Set the cache-header
            res.setHeader("Cache-Control", "public, max-age=5, immutable");
            res.setHeader("X-Cache", "MISS");
            res.status(200).json(data);
        }

        const endTime = performance.now();
        console.log(`Call took ${endTime - startTime} milliseconds`, page);
    } catch (err) {
        res.status(500).json({error: "Server error"});
    }
}



