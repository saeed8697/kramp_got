import IceAndFireApiService from "../../../services/IceAndFireApiService";

let items = {}
/**
 *
 * for retriving detail of character
 * @param req id of char
 * @param res
 * @returns {Promise<void>}
 */
export default async (req, res) => {
    const {page} = req.query;


    if (req.method === 'POST') {
        const {character} = req.body


        items = character
        res.json(character)
        return
    } else {

        //id not in cache load it
        if (Object.keys(items).length === 0) {
            const response = await IceAndFireApiService.getFireAndIceChar("characters", page);
            items = await response.item
        }
        res.json({
            ts: Date.now(),
            items,
        })
    }
}
