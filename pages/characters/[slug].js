import Link from "next/link";
import {GoArrowLeft} from "react-icons/go";
import * as ga from "../../lib/ga";
import axios from "axios";
import {fetchAge} from "../../hooks/useFetch";
import {useQueries} from "react-query";
import GotUtils from "../../utils/GotUtils";
import {useEffect, useState} from "react";

/**
 * detail page of characters
 * @param charecter
 * @returns {JSX.Element}
 * @constructor
 */
export default function DetailPage({query}) {

    const {name, slug, flag} = query

    const [items, setitem] = useState()



    // here i want to show how to hadnle multiple query 
    const results = useQueries([{
        queryKey: ['detail page  ', name], queryFn: () => fetchAge(name.split(' ')[0])
    }, {
        queryKey: "char_detail", queryFn: async () => {
            const res = await axios.get(`/api/getChar/${slug}`)
            return res.data
        }
    },]);
    //find if loading has finished
    const isLoading = results.some(result => result.isLoading)

//fetch detail of char from result => useQueries {0 : estimated age  , 1 : char detail }
    useEffect(() => {
        if (results[1].data?.items) {
            setitem(results[1].data.items)
        }

    }, [results[1]?.data])


//sample for clicking ga
    const gaclick = (url) => {
        ga.event({
            action: "click", params: {
                search_term: url
            }
        })
    }

    return (<div className="min-h-screen text-dark-elements bg-light-bg dark:bg-dark-bg dark:text-light-elements">
        <div className="flex  mx-8  md:mx-16 mt-1 xl:max-w-screen-xl xl:mx-auto">
            <Link href={flag ?? 'characters'}>
                <button
                    onClick={() => gaclick('backbutton')}
                    className="flex items-center px-8 py-4 rounded shadow focus-within:ring bg-light-elements dark:bg-dark-elements focus:outline-none hover:bg-opacity-5">
                    <GoArrowLeft className="mr-4"/> Back
                </button>
            </Link>
        </div>

        {items && !isLoading ?
            <div
                className="max-w-screen-xl gap-24 pb-8 mx-8 xl:mt-24 md:pb-12 lg:pb-0 lg:items-center md:mx-24 xl:px-12 lg:mx-32 xl:flex xl:mx-auto">
                <img
                    className="w-full p-10 h-auto rounded-md md:max-w-2xl xl:max-w-xl"
                    src={"/img/got.png"}
                    alt={`The flag of ${items.name}`}
                />

                <div className="items-center justify-center pt-12 lg:pt-16 xl:p-0 md:flex-row">
                    <h1 className="mb-8 text-2xl font-bold">{items.name ?? 'not mentioned '}</h1>
                    <div className="gap-12 md:flex">
                        <ul className="leading-relaxed">
                            <li>
								<span className="font-semibold">
									Aliases:{" "}
								</span>
                                {items.aliases.length > 0 ? items.aliases.toString() : 'not mentioned'}
                            </li>
                            <li>
								<span className="font-semibold">
									Born:{" "}
								</span>
                                {items.born}
                            </li>
                            <li>
                                <span className="font-semibold">Culture: </span>
                                {items.culture}
                            </li>
                            <li>
								<span className="font-semibold">
									Gender :{" "}
								</span>
                                {items.gender}
                            </li>
                            <li>
                                <span className="font-semibold">Father : </span>
                                {items.father}
                            </li>
                            <li>
                                <span className="font-semibold">Mother: </span>
                                {items.mother}
                            </li>
                        </ul>
                        <ul className="mt-4 leading-relaxed md:mt-0">
                            <li>
								<span className="font-semibold">
									Spouse:{" "}
								</span>
                                {items.spouse}
                            </li>
                            <li>
								<span className="font-semibold">
									PlayedBy:{" "}
								</span>
                                {items.playedBy[0]}
                            </li>
                            <li>
								<span className="font-semibold max-w-full mt-2">
									Books:{" "}
								</span>
                                <br/>

                                {items.books.length > 0 ? items.books
                                    .map((language, i) => (<span key={i}
                                        className="bg-blue-100 ml-3 text-blue-800 text-xs font-medium inline-flex items-center px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">
                            <svg className="mr-1 w-3 h-3" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    fillRule="evenodd"
                                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                                    clipRule="evenodd">

                            </path>
                            </svg>
                                        {GotUtils.getRouteUrl(language)}

                                    </span>)) : 0}


                            < /li>
                        </ul>
                    </div>
                    <h1 className="mb-8 text-2xl mt-2 font-bold">Estimated Age
                        => {results[0]?.data?.age ?? 'not mentioned '}</h1>

                    <div className="mt-8 xl:max-w-lg">
                        <span className="font-semibold">TvSeries: </span>
                        <br/>

                        {items.tvSeries ? (items.tvSeries.map((border, i) => (

                            <button key={i} type="button"
                                    className="inline-flex ml-2 mt-2  items-center px-5 py-2.5 text-sm font-medium text-center text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 text-white dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Session
                                <span
                                    className="inline-flex items-center justify-center w-4 h-4 ml-2 text-xs font-semibold text-blue-800 bg-blue-200 rounded-full">

                                { //extract number
                                    border != "" ?
                                        border.match(/\d+/)[0] : 0
                                }

                            </span>
                            </button>


                        ))) : (<p>None</p>)}


                    </div>
                </div>
            </div>
            : <h1>is loading </h1>}


    </div>);
}

export async function getServerSideProps(context) {

    return {props: {query: context.query},};
}


