import React, {useCallback, useEffect, useState} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import dynamic from "next/dynamic";
import SearchBar from "../../components/SearchBar";
import useInfiniteFetch from "../../hooks/useInfiniteFetch";
import Link from 'next/link'
import GotUtils from "../../utils/GotUtils";
import useMutationDetail from "../../hooks/usseMutation";
//create lazy load on card
const DynamicComponentCardcharecter = dynamic(() => import('../../components/CharacterCard'),
    {loading: () => <p>data is loading (Lazy)</p>})


function InfinitePagination() {


//mutation for handling our state
    const addTodoMutation = useMutationDetail()

//-----------------------------------------------

//query on charactes for pagination

    const {data, status, fetchNextPage, hasNextPage} = useInfiniteFetch()


//-----------------------------------------------
// states for handling  cards

    const [list, setList] = useState([]);
    const [search, setSearch] = useState("");
    const [focused, setFocused] = useState(false)


    useEffect(() => {

        if (data) {

            setList(data?.pages)

        }
    }, [data])


    // create debounce on input changes
    const debounce = (func) => {
        let timer;
        return function (...args) {
            const context = this;
            if (timer) clearTimeout(timer);
            timer = setTimeout(() => {
                timer = null;
                func.apply(context, args);
            }, 500);
        };
    };

    //filter on searches
    const handleChange = (value) => {
        if (value === "") {
            setSearch([])
            return
        }
        if (data) {
            let result = data?.pages.map(a => a.items.items);
            let flatten = Object.values(result).flat();


            const searchdata = flatten
                .filter(
                    (list) =>
                        list.name.toLowerCase().includes(value)
                )


            setSearch(searchdata)
        }


    };

    // create callback for memoized our function
    const optimizedFn = useCallback(debounce(handleChange), [data]);


    return (
        <div>
            <main className="max-w-screen-xl min-h-screen pb-16 mx-8 md:mx-16 xl:mx-auto">
                <div className="flex-row items-center justify-between py-8 sm:flex sm:mb-16 dark:text-white">
                    <SearchBar setSearch={optimizedFn} setFocused={setFocused}/>
                    {search.length > 0 && (
                        <table className="grid">
                            <thead>
                            <tr className='text'>
                                <th className=' pl-5 w-36'>Click</th>
                                <th className=' pl-5 w-36'>ID</th>
                                <th className=' pl-5 w-36'> NAME</th>
                                <th className=' pl-5 w-36'>Played By</th>
                            </tr>
                            </thead>


                            {search.map((el, i) => (
                                <Link

                                    href={{
                                        pathname: '/characters/[slug]',
                                        query: {
                                            slug: GotUtils.getIdFromUrl(el.url),
                                            name: el.name,
                                            flag: '/characters'

                                        },
                                    }}

                                    key={GotUtils.getIdFromUrl(el.url)}

                                >


                                        <tbody className="pt-2 pb-2">
                                        <tr onClick={()=> addTodoMutation.mutate(el)} className='text mt-2 w-24 text-center	border-b'>
                                            <td className='  w-36'>
                                                <button
                                                    className="bg-blue-500 hover:bg-blue-700 text-white   font-bold py-1 px-2 rounded-full ml-5">
                                                    C
                                                </button>
                                            </td>
                                            <td className=' pl-5 w-36'>{GotUtils.getIdFromUrl(el.url)}</td>
                                            <td className=' pl-5 w-36'> {el.name}</td>
                                            <td className=' pl-5 w-36'>{el.playedBy}</td>

                                        </tr>

                                        </tbody>




                                </Link>

                            ))}

                        </table>
                    )}
                </div>

                <hr/>
                {status === "success" && (
                    <InfiniteScroll
                        dataLength={data?.pages.length * 20}
                        next={fetchNextPage}
                        hasMore={hasNextPage}
                        loader={<h4>Loading...</h4>}
                    >
                        <div className="cardwrapper">
                            {list.map((page,i) => (
                                <>
                                    {page.items?.items.map((character) => (
                                        <Link

                                            href={{
                                                pathname: '/characters/[slug]',
                                                query: {
                                                    slug: GotUtils.getIdFromUrl(character.url),
                                                    name: character.name,
                                                    flag: '/characters'
                                                },
                                            }}

                                            key={GotUtils.getIdFromUrl(character.url)}

                                        >

                                            <a onClick={() => {
                                                addTodoMutation.mutate(character)
                                            }}>
                                                <DynamicComponentCardcharecter charecter={character}/>
                                            </a>


                                        </Link>
                                    ))}
                                </>
                            ))}
                        </div>


                    </InfiniteScroll>
                )}
            </main>

        </div>
    );
}

export default InfinitePagination;
