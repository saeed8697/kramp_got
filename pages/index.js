import React, {useState} from "react";
import Link from "next/link";


export default function Mainpage() {
    const [collapsed, setCollapsed] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed);
    return (
        <div>
            <div className="parallax d-none d-lg-flex">
                <div className="parallax-inner">
                    <Link
                        href={"/characters"}
                        className="btn btn-primary px-5"
                        style={{
                            position: "absolute",
                            marginTop: "55px",
                            marginLeft: "36%",
                        }}
                    >
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full mr-4">
                            Infinite Pagination
                        </button>

                    </Link>
                    <Link
                        href={"/TablePaginate"}
                        className="btn btn-success px-5 "
                        style={{
                            position: "absolute",
                            marginTop: "55px",
                            marginLeft: "50%",
                        }}
                    >
                        <button
                            className="bg-amber-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full  ml-4">
                            Table Pagination
                        </button>
                    </Link>
                </div>
            </div>

            <div className="p-5">
                <h2 className="TrendingNow-heading mt-10">Table Pagination  </h2>
                <p className="mt-5 text-gray-700 dark:text-white"> This Page is for handling data inside table on each column you can sort and can see detil by clicking on detail button .
                    we first handle our states with use reducer and fetch our data through react table . we memoized our column value to prevent  rerendering  we use use query to fetch our data through custom local api with redis for caching </p>
                <h2 className="TrendingNow-heading mt-10 ">Infinite Pagination  </h2>
                <p className="mt-5 text-gray-700 dark:text-white">This page is for handling Infinite Scroll behaviour of user . we first fetch our data through react query that call local api supported with  redis  To show how caching perform i placed search input for searching through card we acc use mutation to enable our state management for passing our card info to second component  </p>
                <h2 className="TrendingNow-heading mt-10 ">Detail Pagination   </h2>
                <h2 className=" mt-5 text-gray-700 dark:text-white pb-12 ">This page showing detail information about the character ** if you  came from Table page or character  it will load it from our state and we do not need to  call api again but for static hardcode url it will recall api . we also fetch estimated age on api to show the character age   </h2>


            </div>


        </div>
    );
}
