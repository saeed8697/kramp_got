import GotUtils from "../utils/GotUtils";

class IceAndFireApiService {
    requestObject = {
        method: 'GET',
        headers: {
            'Accept': 'application/vnd.anapioficeandfire+json; version=1'
        }
    }

    // calling api and retrive pagination from api
    static getFireAndIceDetail(type, page = "", pageSize = "25", queryParam = "") {
        let url = `https://www.anapioficeandfire.com/api/${type}${"/"}${"?page=" + page + "&pageSize=" + pageSize}`;

        return fetch(url, this.requestObject).then(response => {
                let regex = /\?([a-zA-Z0-9.?=&:/">; ])+/g
                let paginationLink = response.headers.get('link') || ""
                let paginationList = paginationLink.match(regex) || []

                return response.json().then(items => {
                    if ((Object.keys(items).length > 1 && items.constructor === Object)) {
                        items = [items]
                    }
                    return {
                        "items": items,
                        "next": paginationList.filter(item => item.search('next') > -1),
                        "prev": paginationList.filter(item => item.search('prev') > -1),
                        "first": paginationList.filter(item => item.search('first') > -1),
                        "last": paginationList.filter(item => item.search('last') > -1)
                    }
                })

            }
        )
    }

//get char by id
    static getFireAndIceChar(type, char = "") {
        let url = `https://www.anapioficeandfire.com/api/${type}/${char}`;

        return fetch(url, this.requestObject).then(response => {

                return {
                    "item": response.json(),

                }
            }
        )
    }

    //for retriving data from api and also pass first last perv to handle it on pagination
    static getResultOfSpi(page, size) {
        return IceAndFireApiService.getFireAndIceDetail('characters', page, size)
            .then(items => {

                    if (!items) {
                        return {
                            notFound: true
                        };
                    }
                    items.first = GotUtils.getQueryParamAsObject(items.first).get('page') || "1";
                    items.last = GotUtils.getQueryParamAsObject(items.last).get('page') || "1";
                    items.prev = GotUtils.getQueryParamAsObject(items.prev).get('page') || items.first;
                    items.next = GotUtils.getQueryParamAsObject(items.next).get('page') || items.last;
                    items.current = (parseInt(items.next, 10) === parseInt(items.last, size) && parseInt(items.next, size) - 1 === parseInt(items.prev, size))
                        ? (parseInt(items.last, size)).toString()
                        : (parseInt(items.next, size) - 1 < parseInt(items.last, size))
                            ? (parseInt(items.next, size) - 1).toString()
                            : "1"


                    return {
                        items,
                    };
                }
            )


    }
}

export default IceAndFireApiService